### PAM

- **P**luggable **A**uthentication **M**odule is a framework provided by Linux for user authentication across the whole system. 
- Deleting PAM config `/etc/pam.d/*` and `/etc/pam.conf` could potentially lock you out of your system.

- Modules can be implemented through libpam.so

```bash
λ ~/ ldd /usr/bin/login
	linux-vdso.so.1 (0x00007ffc7f8f4000)
	libpam.so.0 => /usr/lib/libpam.so.0 (0x00007f2fd25e6000)
	libpam_misc.so.0 => /usr/lib/libpam_misc.so.0 (0x00007f2fd25e1000)
	libc.so.6 => /usr/lib/libc.so.6 (0x00007f2fd23d7000)
	libaudit.so.1 => /usr/lib/libaudit.so.1 (0x00007f2fd23ae000)
	libdl.so.2 => /usr/lib/libdl.so.2 (0x00007f2fd23a9000)
	/lib64/ld-linux-x86-64.so.2 => /usr/lib64/ld-linux-x86-64.so.2 (0x00007f2fd261f000)
	libcap-ng.so.0 => /usr/lib/libcap-ng.so.0 (0x00007f2fd23a0000)
```

- PAM (through libpam.so) acts as an interface between the client who's requesting for authentication on behalf of a user and the PAM module on the other side which is an abstraction for implementations of authentication like username/password (pam\_unix), fingerprint (pam\_fprintd), Kerberos (pam\_krb5).

<center>

```mermaid
flowchart LR
    A(Client) -->|libpam.so| B(PAM) 
	F(PAM Rules) -.-o B
    B -.-> C(pam_unix)
    B -.-> D(pam_fprintd)
    B -.-> E(pam_krb5)
```

</center>

- Post authenticate, a session is created which is torn down (by the PAM) when the session ends (e.g. in case a desktop session, when the user logs out)
- The sequence of PAM operations are encapsulated within a transaction. The first PAM function that a client should call is `pam_start` which initiates a transaction. On successful return, this function returns a PAM handle required to call further functions. 

- A PAM Policy specifies the service using PAM (can also be specified by the filename, e.g. /etc/pam.d/login), a module type, a control directive that indicates the stacking order for the module.

```bash
λ ~/ /bin/cat /etc/pam.d/login
#%PAM-1.0
auth       required     pam_securetty.so
auth       requisite    pam_nologin.so
auth       include      system-local-login
account    include      system-local-login
session    include      system-local-login
password   include      system-local-login
```

- Module Type defines the interface that the module offers.
	- `auth`: Authenticate the user using password, biometrics etc.
	- `account`: Verify if user should be able/allowed to access the account.
	- `password`: Passwords are abstracted behind "tokens", this type of module offers management of tokens.
	- `session`: Such a module is responsible for managing configuration

- The control flags determine the priority of modules
	- 

##### References

1. [https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam](https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam)
2. [https://www.netbsd.org/docs/guide/en/chap-pam.html](https://www.netbsd.org/docs/guide/en/chap-pam.html)
