### SELinux

SELinux is a Linux kernel module that adds support for access control based policies. Since the policy is a separate part of the system itself, the access control it enforces is different from typical Linux file permissions that can be modified by the user and by programs.

Linux, otherwise, has Directory Access Control (DAC), meaning access to directories and files is controlled using user and group identities. Misconfigured programs or accidental privilege escalation could mean bypassing these restrictions.

The [Arch wiki gives two good examples](https://wiki.archlinux.org/title/SELinux) on how SELinux is different from standard linux file and directory permissions:

> An example would be the use of the sudo command. When DACs are enforced, sudo allows temporary privilege escalation to root, giving the process so spawned unrestricted systemwide access. However, when using MACs, if the security administrator deems the process to have access only to a certain set of files, then no matter what the kind of privilege escalation used, unless the security policy itself is changed, the process will remain constrained to simply that set of files. So if sudo is tried on a machine with SELinux running in order for a process to gain access to files its policy does not allow, it will fail.

> Another set of examples are the traditional (-rwxr-xr-x) type permissions given to files. When under DAC, these are user-modifiable. However, under MAC, a security administrator can choose to freeze the permissions of a certain file by which it would become impossible for any user to change these permissions until the policy regarding that file is changed.

Using an SELinux policy, one can typically restrict a process to access only the resources it needs to and nothing else.

Android has had SELinux available since version 4.4. Progressively the policy has improved to restrict capabilities for applications and even system services. However, individual OEMs can modify that policy as they wish.


SELinux has 3 modes:
1. Disabled: SELinux is completely disabled.
2. Permissive: SELinux is disabled but a policy is available and actions that would otherwise have been denied according to the policy are logged.
3. Enabled: SELinux is enabled and will enforce the installed policy.

A set of rules define a SELinux policy. Instead of explaining a policy using an example, the LineageOS team has a great writeup on [how Android's SELinux policy is defined](https://lineageos.org/engineering/HowTo-SELinux/) right down to links to the source code.


---

#### References

1. [https://lwn.net/Articles/609511/](https://lwn.net/Articles/609511/)
2. [https://en.wikipedia.org/wiki/Security-Enhanced_Linux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux)
3. [https://lwn.net/Articles/103230/](https://lwn.net/Articles/103230/)
4. [https://lineageos.org/engineering/HowTo-SELinux/](https://lineageos.org/engineering/HowTo-SELinux/)
