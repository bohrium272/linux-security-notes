### AppArmor

AppArmor is a Linux Kernel security extension that allows Mandatory Access Control (MAC) on a system. It supplements the Directory Access Control that Linux has by default.

Given the complexity of configuring SELinux (an alternative MAC), most distributions ship with AppArmor.

Considering an HTTP server and reverse-proxy like Nginx, Docker's documentation shows an example policy that gives the Nginx process restrictive permissions.

```
#include <tunables/global>        # Directive to include all default tunables, check https://www.apt-browse.org/browse/ubuntu/precise/main/amd64/apparmor/2.7.102-0ubuntu3/file/etc/apparmor.d/tunables

profile docker-nginx flags=(attach_disconnected,mediate_deleted) {
    #include <abstractions/base>

    network inet tcp,             # allow TCP connections
    network inet udp,             # allow UDP connections
    network inet icmp,            # allow ICMP communication

    deny network raw,             # deny raw type network sockets

    deny network packet,          # deny access to network packets

    file,                         # grants access to all files
    umount,                       # unmount any mounted directory

    deny /bin/** wl,              # deny access to certain files using a glob syntax
    deny /boot/** wl,
    deny /dev/** wl,
    deny /etc/** wl,
    deny /home/** wl,
    deny /lib/** wl,
    deny /lib64/** wl,
    deny /media/** wl,
    deny /mnt/** wl,
    deny /opt/** wl,
    deny /proc/** wl,
    deny /root/** wl,
    deny /sbin/** wl,
    deny /srv/** wl,
    deny /tmp/** wl,
    deny /sys/** wl,
    deny /usr/** wl,

    audit /** w,                  # audit keyword means the mentioned operation to files matching the glob will be logged

    /var/run/nginx.pid w,         # the allow keyword is implicit

    /usr/sbin/nginx ix,

    deny /bin/dash mrwklx,
    deny /bin/sh mrwklx,
    deny /usr/bin/top mrwklx,


    capability chown,            # grant capabilities to the process, `capabilities,` implies granting all capabilities 
    capability dac_override,
    capability setuid,
    capability setgid,
    capability net_bind_service,

    deny @{PROC}/* w,            # deny write for all files directly in /proc (not in a subdir)
    # deny write to files not in /proc/<number>/** or /proc/sys/**
    deny @{PROC}/{[^1-9],[^1-9][^0-9],[^1-9s][^0-9y][^0-9s],[^1-9][^0-9][^0-9][^0-9]*}/** w,
    deny @{PROC}/sys/[^k]** w,  # deny /proc/sys except /proc/sys/k* (effectively /proc/sys/kernel)
    deny @{PROC}/sys/kernel/{?,??,[^s][^h][^m]**} w,  # deny everything except shm* in /proc/sys/kernel/
    deny @{PROC}/sysrq-trigger rwklx,
    deny @{PROC}/mem rwklx,
    deny @{PROC}/kmem rwklx,     # Variables can either be defined in the configuration or some included variables can be imported
    deny @{PROC}/kcore rwklx,    # https://www.apt-browse.org/browse/ubuntu/precise/main/amd64/apparmor/2.7.102-0ubuntu3/file/etc/apparmor.d/tunables

    deny mount,                  # Deny ability to mount directories

    deny /sys/[^f]*/** wklx,
    deny /sys/f[^s]*/** wklx,
    deny /sys/fs/[^c]*/** wklx,
    deny /sys/fs/c[^g]*/** wklx,
    deny /sys/fs/cg[^r]*/** wklx,
    deny /sys/firmware/** rwklx,
    deny /sys/kernel/security/** rwklx,
}
```

Similar to SELinux, AppArmor also supports two modes:
1. Enforce: The configured policy is enforced and violations are reported through logs.
2. Complain: The configured policy is not enforced but violations of that policy are still reported through logs.


#### Bypass

A flaw in `runc` allowed bypassing/overwriting the AppArmor profile if a container mounted a controlled directory e.g. the `/proc` directory [https://github.com/opencontainers/runc/issues/2128](https://github.com/opencontainers/runc/issues/2128)
